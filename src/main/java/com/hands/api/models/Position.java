package com.hands.api.models;

public class Position {
	private final double latitude;
	private final double longitude;

	public Position(double lat, double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}

	public double getLat() {
		return latitude;
	}

	public double getLng() {
		return longitude;
	}
}
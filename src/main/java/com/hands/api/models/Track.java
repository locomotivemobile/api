package com.hands.api.models;

import com.hands.api.models.Position;

public class Track {
	private final String uid;
	private final String userId;
	private final String deviceOs;
	private final String deviceOsVersion;
	private final String deviceModel;
	private final String address;
	private final Position home;
	private final Position work;
	private final Position place;

	public Track(String id, String userId, Position home, Position work, String deviceOs, String deviceModel, String osVersion, String address, Position place) {
        this.uid = id;
        this.userId = userId;
        this.deviceOs = deviceOs;
        this.deviceModel = deviceModel;
        this.deviceOsVersion = osVersion;
        this.home = home;
        this.work = work;
        this.place = place;
        this.address = address;
    }

	public String getId() {
		return uid;
	}

	public String getUserId() {
		return userId;
	}

	public String getAddress() {
		return address;
	}

	public String getDeviceOs() {
		return deviceOs;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public String getDeviceOsVersion() {
		return deviceOsVersion;
	}

	public Position getHome() {
		return home;
	}

	public Position getWork() {
		return work;
	}

	public Position getPlace() {
		return place;
	}
}
package com.hands.api;

import com.coxautodev.graphql.tools.SchemaParser;
import javax.servlet.annotation.WebServlet;
import graphql.servlet.SimpleGraphQLServlet;
import graphql.schema.GraphQLSchema;
import com.hands.api.repository.TrackRepository;
import com.hands.api.Scalars;

import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import java.util.List;
import graphql.GraphQLError;
import graphql.ExceptionWhileDataFetching;
import graphql.servlet.GraphQLContext;

import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

	private static final TrackRepository trackRepository;

	static {
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://aeclean:*loc1332@ds231719.mlab.com:31719/heroku_5k77q6q1"));
        MongoDatabase mongo = mongoClient.getDatabase("heroku_5k77q6q1");
        trackRepository = new TrackRepository(mongo.getCollection("Track"));
    }

	public GraphQLEndpoint() {
		super(buildSchema());
	}

	private static GraphQLSchema buildSchema() {
		return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(
                    new Query(trackRepository),
                	new TrackResolver(trackRepository)
                )
                .build()
                .makeExecutableSchema();
	}

	@Override
    protected List<GraphQLError> filterGraphQLErrors(List<GraphQLError> errors) {
        System.out.println(errors);
        return errors.stream()
            .filter(e -> e instanceof ExceptionWhileDataFetching || super.isClientError(e))
            .map(e -> e instanceof ExceptionWhileDataFetching ? new SanitizedError((ExceptionWhileDataFetching) e) : e)
            .collect(Collectors.toList());
    }
}
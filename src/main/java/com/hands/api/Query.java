package com.hands.api;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import com.hands.api.repository.TrackRepository;
import com.hands.api.models.Track;
import com.hands.api.filters.TrackFilter;
import java.util.List;

public class Query implements GraphQLRootResolver {
    
    private final TrackRepository trackRepository;

    public Query(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    public List<Track> allTracks(TrackFilter filter, Number skip, Number first) {
        return trackRepository.getAllTracks(filter, skip.intValue(), first.intValue());
    }
    public Track getTrack(String trackId) {
        return trackRepository.getTrack(trackId);
    }
}
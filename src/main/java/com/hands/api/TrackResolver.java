package com.hands.api;

import com.hands.api.models.Track;
import com.hands.api.repository.TrackRepository;
import com.coxautodev.graphql.tools.GraphQLResolver;

public class TrackResolver implements GraphQLResolver<Track> {
	private final TrackRepository trackRepository;

    TrackResolver(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    public String userId(Track track) {
    	if(track.getUserId() != null) {
    		return track.getUserId();
    	}
    	return null;
    }
}
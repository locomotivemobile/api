package com.hands.api.filters;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackFilter {
	private String osContains;
    private String deviceModelContains;

    @JsonProperty("os_contains")
    public String getOsContains() {
        return osContains;
    }

    public void setOsContains(String osContains) {
        this.osContains = osContains;
    }

    @JsonProperty("device_model_contains")
    public String getDeviceModelContains() {
        return deviceModelContains;
    }

    public void setDeviceModelContains(String deviceModelContains) {
        this.deviceModelContains = deviceModelContains;
    }
}
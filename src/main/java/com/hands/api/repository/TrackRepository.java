package com.hands.api.repository;

import java.util.*;
import com.hands.api.models.Track;
import com.hands.api.filters.TrackFilter;
import com.hands.api.models.Position;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.regex;

public class TrackRepository {
    
    private final MongoCollection<Document> tracks;

    public TrackRepository(MongoCollection<Document> tracks) {
        this.tracks = tracks;
    }

    public Track findById(String id) {
        Document doc = tracks.find(eq("_id", new ObjectId(id))).first();
        return track(doc);
    }

    public List<Track> getAllTracks(TrackFilter filter, int skip, int first) {
        Optional<Bson> mongoFilter = Optional.ofNullable(filter).map(this::buildFilter);

        List<Track> allTracks = new ArrayList<>();
        FindIterable<Document> documents = mongoFilter.map(tracks::find).orElseGet(tracks::find);
        for(Document doc: documents.skip(skip).limit(first)) {
            allTracks.add(track(doc));
        }
        
        return allTracks;
    }

    public Track getTrack(String trackId) {
        return findById(trackId);
    }
    
    public void saveTrack(Track track) {
        Document doc = new Document();
        
        doc.append("userId", track.getUserId());
        doc.append("euid", track.getUserId());
        doc.append("os", track.getDeviceOs());
        doc.append("deviceModel", track.getDeviceModel());

        tracks.insertOne(doc);
    }

    private Bson buildFilter(TrackFilter filter) {
        String osPattern = filter.getOsContains();
        String modelPattern = filter.getDeviceModelContains();
        Bson osCondition = null;
        Bson modelCondition = null;
        
        if (osPattern != null && !osPattern.isEmpty()) {
            osCondition = regex("os", ".*" + osPattern + ".*", "i");
        }
        
        if (modelPattern != null && !modelPattern.isEmpty()) {
            modelCondition = regex("deviceModel", ".*" + modelPattern + ".*", "i");
        }
        
        if (osCondition != null && modelCondition != null) {
            return and(osCondition, modelCondition);
        }
        return osCondition != null ? osCondition : modelCondition;
    }
    
    private Track track(Document doc) {
        System.out.println(doc);

        Document home = (Document)doc.get("home");
        Document work = (Document)doc.get("work");
        Document place = (Document)doc.get("venueLngLat");

        return new Track(
            doc.get("_id").toString(),
            doc.getString("euid"),
            new Position(home.getDouble("lat"), home.getDouble("lng")),
            new Position(work.getDouble("lat"), work.getDouble("lng")),
            doc.getString("os"),
            doc.getString("deviceModel"),
            doc.getString("osVersion"),
            doc.getString("address"),
            new Position(place.getDouble("lat"), place.getDouble("lng")));
    }
}